import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out audio!" )

# Load music and sound files
music = pygame.mixer.music.load( "content/audio/DancingBunnies_Moosader.ogg" )
sfx = pygame.mixer.Sound( "content/audio/powerup.wav" )

# Play the music BEFORE the game loop starts
pygame.mixer.music.play()

# Game loop
done = False
while done == False:
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True

        if ( event.type == KEYDOWN ):                
            if ( event.key == K_DOWN ):
                sfx.set_volume( sfx.get_volume() - 0.1 )
                print( "Volume: " + str( vol ) )
                sfx.play()

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
