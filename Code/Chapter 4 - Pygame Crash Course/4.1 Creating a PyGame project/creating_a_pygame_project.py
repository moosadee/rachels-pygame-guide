import pygame
from pygame.locals import *

# Global variables
screenWidth = 300
screenHeight = 300
timer = None
window = None
fps = 30
bgColor = pygame.Color( 50, 200, 255 )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

# Function definitions
def InitPygame( screenWidth, screenHeight ):
    global window
    global timer
    pygame.init()
    timer = pygame.time.Clock()
    window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
    pygame.display.set_caption( "Testing out PyGame!" )

# Program start
InitPygame( screenWidth, screenHeight )

# Game loop
done = False
while done == False:
    window.fill( bgColor )

    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True

    # Draw images
    window.blit( imgGrass, imgGrass.get_rect() )
    window.blit( imgBunny, imgBunny.get_rect() )

    # Update screen
    pygame.display.update()
    timer.tick( fps )






