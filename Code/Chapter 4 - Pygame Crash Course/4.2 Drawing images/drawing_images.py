import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out images!" )
bgColor = pygame.Color( 50, 200, 255 )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

bunnyRect = pygame.Rect( 200, 100, 64, 64 )

# Game loop
done = False
while done == False:
    window.fill( bgColor )

    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
        if ( event.type == KEYDOWN ):                
            if ( event.key == K_DOWN ):
                bunnyRect.y = bunnyRect.y + 10

    # Draw images
    window.blit( imgGrass, imgGrass.get_rect() )
    window.blit( imgBunny, bunnyRect )

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
