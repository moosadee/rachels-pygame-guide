import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out smooth keyboard input!" )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

bunnyRect = pygame.Rect( 200, 100, 64, 64 )

done = False
while ( done == False ):    
    # Events
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
    # Smooth keyboard movement
    keys = pygame.key.get_pressed()
    if ( keys[ K_UP ] ):
        bunnyRect.y = bunnyRect.y - 5
    elif ( keys[ K_DOWN ] ):
        bunnyRect.y = bunnyRect.y + 5
    elif ( keys[ K_LEFT ] ):
        bunnyRect.x = bunnyRect.x - 5
    elif ( keys[ K_RIGHT ] ):
        bunnyRect.x = bunnyRect.x + 5
    
    # Draw images
    window.blit( imgGrass, imgGrass.get_rect() )
    window.blit( imgBunny, bunnyRect )

    pygame.display.update()
    timer.tick( 30 )
