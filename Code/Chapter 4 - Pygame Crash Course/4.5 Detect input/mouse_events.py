import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out events!" )

# Game loop
done = False
while done == False:    
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
        elif ( event.type == MOUSEBUTTONDOWN ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE DOWN EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            
        elif ( event.type == MOUSEBUTTONUP ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE UP EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )
            
            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            elif ( event.button == 4 ):
                print( "\t SCROLL UP" )
            elif ( event.button == 5 ):
                print( "\t SCROLL DOWN" )

        elif ( event.type == MOUSEMOTION ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE MOVE EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
