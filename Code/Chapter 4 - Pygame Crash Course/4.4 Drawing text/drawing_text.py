import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out text!" )

textColor = pygame.Color( 255, 255, 255 )

mainFont = pygame.font.Font( "content/fonts/DancingScript-Regular.ttf", 30 )
textSurface = mainFont.render( "Hello, world!", False, textColor )

# Game loop
counter = 0
done = False
while done == False:
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
        if ( event.type == KEYDOWN ):                
            if ( event.key == K_DOWN ):
                counter = counter + 1
                textSurface = mainFont.render( str( counter ), False, textColor )

    # Draw text
    window.blit( textSurface, textSurface.get_rect() )

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
