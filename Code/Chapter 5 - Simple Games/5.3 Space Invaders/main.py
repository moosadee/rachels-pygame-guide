import pygame
from pygame.locals import *

from application import Application
from character import Character

app = Application()
app.CreateWindow( "Pickin' Sticks", 600, 400 )

imgGirl = pygame.image.load( "graphics/Ayne.png" )
imgGrass = pygame.image.load( "graphics/Terrain.png" )

player = Character()
player.SetImage( imgGirl )
player.SetPosition( app.GetScreenWidth() / 2, app.GetScreenHeight() / 2 )

def DrawGrass( app, image ):
    for y in range( 0, app.GetScreenHeight() / 64 + 1 ):
        for x in range( 0, app.GetScreenWidth() / 64 + 1 ):
            app.GetWindow().blit( image, (x * 64, y * 64) )

while ( app.IsDone() == False ):
    app.CycleBegin()
    
    # Events
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            app.Quit()
    
    # Key presses
    keys = pygame.key.get_pressed()
    
    player.Move( keys )
    
    DrawGrass( app, imgGrass )
    player.Draw( app.GetWindow() )
    
    app.CycleEnd()

# End of program
app.Cleanup()
