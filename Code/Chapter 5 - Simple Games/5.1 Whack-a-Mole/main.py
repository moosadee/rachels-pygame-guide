import pygame
from pygame.locals import *
import random

# Our custom classes
from application import Application
from assetsManager import AssetsManager
from mole import Mole

app = Application()
app.CreateWindow( "Whack-a-Mole", 500, 500 )

assets = AssetsManager()

# Score variable
points = 0

# Mole event counter
counter = 0

scoreSurface = assets.CreateTextSurface( "Score: " + str( points ), "main", pygame.Color( 0, 0, 0 ) )

# Create a list of holes
moles = []
for y in range( 3 ):
    for x in range( 3 ):
        mole = Mole()
        mole.SetImages( assets.GetImage( "hole_empty" ), assets.GetImage( "hole_mole" ), assets.GetImage( "hole_hurt" ) )
        mole.SetPosition( x * 150 + 40, y * 150 + 30 )
        moles.append( mole ) # Add to list of moles

assets.PlayMusic( "content/audio/ClearDay_Moosader.ogg" )
while ( app.IsDone() == False ):
    app.CycleBegin()

    counter = counter + 1

    # Randomly pop a mole
    if ( counter % 10 == 0 ):
        whichMole = random.randint( 0, 8 )
        moles[ whichMole ].PopMole()
    
    # Events
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            app.Quit()

        if ( event.type == MOUSEBUTTONDOWN ):
            mouseX, mouseY = event.pos

            # Check all the holes
            for mole in moles:
                if ( mole.IsClicked( mouseX, mouseY ) and mole.GetState() == "mole" ):
                    mole.Hurt()
                    assets.PlaySound( "hit" )
                    points = points + 1
                    scoreSurface = assets.CreateTextSurface( "Score: " + str( points ), "main", pygame.Color( 0, 0, 0 ) )

    if ( points == 10 ):
        # Game over
        scoreSurface = assets.CreateTextSurface( "You win!", "main", pygame.Color( 0, 0, 0 ) )

    app.GetWindow().blit( assets.GetImage( "background" ), ( 0, 0 ) )
    app.GetWindow().blit( scoreSurface, ( 0, 0 ) )

    # Update the moles
    for mole in moles:
        mole.Update()
        mole.Draw( app.GetWindow() )
    
    app.CycleEnd()

# End of program
app.Cleanup()
