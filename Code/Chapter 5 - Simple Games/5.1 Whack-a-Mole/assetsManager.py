import pygame

class AssetsManager:
    def __init__( self ):
        self.images = {}
        self.fonts = {}
        self.sounds = {}

        self.LoadImages()
        self.LoadFonts()
        self.LoadSounds()

    def LoadImages( self ):
        self.images[ "background" ] = pygame.image.load( "content/graphics/background.png" )
        self.images[ "hole_empty" ] = pygame.image.load( "content/graphics/hole_empty.png" )
        self.images[ "hole_mole" ]  = pygame.image.load( "content/graphics/hole_mole.png" )
        self.images[ "hole_hurt" ]  = pygame.image.load( "content/graphics/hole_mole_hurt.png" )
        
    def LoadFonts( self ):
        self.fonts[ "main" ] = pygame.font.Font( "content/fonts/Averia-Bold.ttf", 30 )
        
    def LoadSounds( self ):
        self.sounds[ "hit" ] = pygame.mixer.Sound( "content/audio/hit.wav" )

    def GetImage( self, key ):
        return self.images[ key ]
        
    def GetFont( self, key ):
        return self.fonts[ key ]
        
    def PlaySound( self, key ):
        self.sounds[ key ].play()
        
    def PlayMusic( self, path ):
        pygame.mixer.music.load( path )
        pygame.mixer.music.play()

    def CreateTextSurface( self, text, fontKey, color ):
        return self.fonts[ fontKey ].render( text, False, color )
