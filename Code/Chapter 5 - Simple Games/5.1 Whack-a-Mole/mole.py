import random

class Mole:
    def __init__( self ):
        self.x = 0
        self.y = 0
        self.width = 108
        self.height = 94
        self.state = "empty"
        self.imageEmpty = None
        self.imageMole = None
        self.imageMoleHurt = None
        self.countdown = 0

    def SetPosition( self, x, y ):
        self.x = x
        self.y = y

    def SetImages( self, empty, mole, hurt ):
        self.imageEmpty = empty
        self.imageMole = mole
        self.imageMoleHurt = hurt

    def PopMole( self ):
        if ( self.state == "empty" ):
            self.state = "mole"
            self.countdown = random.randint( 10, 50 )

    def GetState( self ):
        return self.state

    def Hurt( self ):
        self.state = "hurt"
        self.countdown = 5

    def Update( self ):
        if ( self.countdown > 0 ):
            self.countdown = self.countdown - 1

            if ( self.countdown == 0 and ( self.state == "hurt" or self.state == "mole" ) ):
                self.state = "empty"

    def IsClicked( self, mouseX, mouseY ):
        # Is mouse (x,y) within the image?
        return ( self.x <= mouseX and mouseX <= self.x + self.width
            and self.y <= mouseY and mouseY <= self.y + self.height )

    def Draw( self, window ):
        if ( self.state == "empty" ):
            window.blit( self.imageEmpty, ( self.x, self.y ) )
            
        elif ( self.state == "mole" ):
            window.blit( self.imageMole, ( self.x, self.y ) )
            
        elif ( self.state == "hurt" ):
            window.blit( self.imageMoleHurt, ( self.x, self.y ) )
            
