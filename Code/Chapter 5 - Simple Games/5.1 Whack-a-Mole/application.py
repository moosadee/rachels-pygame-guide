import pygame
from pygame.locals import *

class Application:
    def __init__( self ):
        pygame.init()
        self.screenWidth = None
        self.screenHeight = None
        self.title = None
        self.timer = pygame.time.Clock()
        self.bgColor = pygame.Color( 50, 50, 50 )
        self.done = False
        self.fps = 30
    
    def Cleanup( self ):
        pygame.quit()
    
    def CreateWindow( self, title, width, height ):
        self.title = title
        self.screenWidth = width
        self.screenHeight = height
        self.window = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )
        pygame.display.set_caption( self.title )
        
    def CycleBegin( self ):
        self.window.fill( self.bgColor )
        
    def CycleEnd( self ):            
        pygame.display.update()
        self.timer.tick( self.fps )
    
    def IsDone( self ):
        return self.done
    
    def Quit( self ):
        self.done = True
    
    def GetScreenWidth( self ):
        return self.screenWidth
    
    def GetScreenHeight( self ):
        return self.screenHeight
    
    def GetWindow( self ):
        return self.window
