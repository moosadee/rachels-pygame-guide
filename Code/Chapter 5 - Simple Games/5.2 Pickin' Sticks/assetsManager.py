import pygame

class AssetsManager:
    def __init__( self ):
        self.images = {}
        self.fonts = {}
        self.sounds = {}

        self.LoadImages()
        self.LoadFonts()
        self.LoadSounds()

    def LoadImages( self ):
        self.images[ "grass" ] = pygame.image.load( "content/graphics/Terrain.png" )
        self.images[ "girl" ] = pygame.image.load( "content/graphics/Ayne.png" )
        self.images[ "stick" ]  = pygame.image.load( "content/graphics/stick.png" )
        
    def LoadFonts( self ):
        self.fonts[ "main" ] = pygame.font.Font( "content/fonts/Gidole-Regular.ttf", 30 )
        
    def LoadSounds( self ):
        self.sounds[ "pickup" ] = pygame.mixer.Sound( "content/audio/pickup.wav" )

    def GetImage( self, key ):
        return self.images[ key ]
        
    def GetFont( self, key ):
        return self.fonts[ key ]
        
    def PlaySound( self, key ):
        self.sounds[ key ].play()
        
    def PlayMusic( self, path ):
        pygame.mixer.music.load( path )
        pygame.mixer.music.play()

    def CreateTextSurface( self, text, fontKey, color ):
        return self.fonts[ fontKey ].render( text, False, color )
