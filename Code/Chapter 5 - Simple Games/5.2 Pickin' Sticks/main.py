import pygame
from pygame.locals import *

import random, math

# Custom objects
from application import Application
from assetsManager import AssetsManager
from character import Character

app = Application()
app.CreateWindow( "Pickin' Sticks", 600, 400 )

assets = AssetsManager()

# player Character
player = Character()
player.SetImage( assets.GetImage( "girl" ) )
player.SetPosition( app.GetScreenWidth() / 2, app.GetScreenHeight() / 2 )

# stick object
stick = Character()
stick.SetImage( assets.GetImage( "stick" ) )
stick.SetPosition( random.randint( 0, app.GetScreenWidth() - 50 ), random.randint( 0, app.GetScreenHeight() - 50 ) )

scoreSurface = assets.CreateTextSurface( "Score: " + str( player.GetScore() ), "main", pygame.Color( 255, 255, 255 ) )

def DrawGrass( app, image ):
    for y in range( 0, app.GetScreenHeight() / 64 + 1 ):
        for x in range( 0, app.GetScreenWidth() / 64 + 1 ):
            app.GetWindow().blit( image, (x * 64, y * 64) )

def GetDistance( player, stick ):
    xDist = player.x - stick.x
    yDist = player.y - stick.y
    return math.sqrt( xDist * xDist + yDist * yDist )

assets.PlayMusic( "content/audio/SuburbanDinosaur_Moosader.ogg")
while ( app.IsDone() == False ):
    app.CycleBegin()
    
    # Events
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            app.Quit()
    
    # Key presses
    keys = pygame.key.get_pressed()

    # Check distance
    if ( GetDistance( player, stick ) <= 20 ):
        assets.PlaySound( "pickup" )
        player.IncrementScore( 1 )
        scoreSurface = assets.CreateTextSurface( "Score: " + str( player.GetScore() ), "main", pygame.Color( 255, 255, 255 ) )
        stick.SetPosition( random.randint( 0, app.GetScreenWidth() - 50 ), random.randint( 0, app.GetScreenHeight() - 50 ) )
    
    if ( player.GetScore() == 10 ):
        # Game over
        scoreSurface = assets.CreateTextSurface( "You win!", "main", pygame.Color( 255, 255, 255 ) )
        
    player.Move( keys )
    
    DrawGrass( app, assets.GetImage( "grass" ) )
    player.Draw( app.GetWindow() )
    stick.Draw( app.GetWindow() )

    app.GetWindow().blit( scoreSurface, ( 0, 0 ) )
    
    app.CycleEnd()

# End of program
app.Cleanup()
