import pygame

class Character:
    def __init__( self ):
        self.score = 0
        
        self.walkingSpeed = 3
        self.runningSpeed = 5
        self.speed = self.walkingSpeed
        
        self.direction = 0
        self.frame = 0
        self.maxFrame = 4
        self.image = None
        
        self.x = 0
        self.y = 0
        self.width = 32
        self.height = 48
        
    def SetImage( self, image ):
        self.image = image
    
    def SetPosition( self, x, y ):
        self.x = x
        self.y = y
    
    def SetDimensions( self, width, height ):
        self.width = width
        self.height = height
    
    def IncrementScore( self, amount ):
        self.score = self.score + amount

    def GetScore( self ):
        return self.score
    
    def Move( self, key ):
        # Check for running
        if ( key[ pygame.K_LSHIFT ] ):
            self.speed = self.runningSpeed
        else:
            self.speed = self.walkingSpeed
        
        # Check for movement
        move = True
        if ( key[ pygame.K_LEFT ] ):
            self.MoveLeft()
        elif ( key[ pygame.K_RIGHT ] ):
            self.MoveRight()
        elif ( key[ pygame.K_UP ] ):
            self.MoveUp()
        elif ( key[ pygame.K_DOWN ] ):
            self.MoveDown()
        else:
            move = False
            
        # Animate if moving
        if ( move == True ):
            self.Animate()
    
    def Animate( self ):
        self.frame = self.frame + 0.1
        if ( self.frame >= self.maxFrame ):
            self.frame = 0

    def MoveUp( self ):
        self.direction = 1
        self.y = self.y - self.speed
        
    def MoveDown( self ):
        self.direction = 0
        self.y = self.y + self.speed
        
    def MoveLeft( self ):
        self.direction = 2
        self.x = self.x - self.speed
        
    def MoveRight( self ):
        self.direction = 3
        self.x = self.x + self.speed
        
    def Draw( self, window ):
        rect = self.image.get_rect()
        rect.x = int( self.frame ) * self.width
        rect.y = self.direction * self.height
        rect.width = self.width
        rect.height = self.height
    
        window.blit( self.image, ( self.x, self.y ), rect )
        



