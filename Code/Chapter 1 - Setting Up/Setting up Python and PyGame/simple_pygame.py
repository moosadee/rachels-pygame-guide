import pygame, sys
from pygame.locals import *

pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out PyGame!" )

# Game loop
while True:
    window.fill( pygame.Color( 50, 200, 255 ) )

    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
