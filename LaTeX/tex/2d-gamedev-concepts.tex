\documentclass[../Intro-to-Pygame.tex]{subfiles}
 
\begin{document}

\chapter{ 2D Game Development Concepts }
            
\section{ Birds-eye-view: How game-program work }

    If you've taken a programming class before, you might
    already understand that a program runs
    \textbf{ from the top $ \to $ down }, line-by-line,
    until it hits the end of the source code file.

    While it's stepping through code, it will pause when
    waiting for the user's \textbf{ input }, then resume once it has it.

    Once the end of the file is hit, the program automatically
    quits, unless you've added some kind of \textbf{ loop } in there to
    keep it running.

    ~\\ If we don't want a program to quit automatically,
    we will usually put a \textbf{ program loop } somewhere
    within, where the same kinds of tasks are done every cycle...

    This could be displaying a menu, having the user select where
    to go, and doing some operation, and then returning back to the menu.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[width=0.7\textwidth]{images/programloop.png}
            \\ \footnotesize A program loop; The menu has three options: (1) Add, (2) Subtract, and (3) Exit.
            \\ If the user selects (1), it goes to the Add function, then returns to the menu.
            \\ If the user selects (2), it goes to the Subtract function, then returns to the menu.
            \\ If the user selects (3), it leaves the loop and exits the program.
        \end{center}
    \end{figure}

    Similarly, a game isn't very \textit{ playable } if it
    exits immediately once you launch it. A game also has
    a \textbf{ game loop } where some common events are
    done every \textbf{ cycle } of the game.

\newpage
\begin{figure}[h]
    \begin{center}
        \includegraphics[width=1.0\textwidth]{images/gameloop.png}
        \\ \footnotesize A program loop for a game;
        First, there is the SETUP section. After SETUP, we
        enter the game loop, which contains: ``handle player input",
        ``update game objects", and ``draw to the screen". 
        If the user quits, then we will leave the loop,
        clean up the game, and exit the program.
    \end{center}
\end{figure}

    During a game, the set of code it does first - \textit{ before}
    the game loop begins - is \textbf{ setup or initialization } code.
    This is where variables are \textbf{ initialized}, assets are
    loaded into memory, and things get prepared for the game.

    Once it enters the game loop, the game will always
    \textbf{ check for player input}
    (Did the player press the ``right" arrow key? Did the player click a button?),
    \textbf{ update the game objects}
    (Move all enemies towards the player by 5 pixels), and
    \textbf{ draw to the screen}
    (Draw the background, the world, the player, and the enemies).
    We will write code for each of these types of events,
    but PyGame will help us out.
    ~\\

    Of course, we can also write \textbf{ functions } to
    handle specific tasks, but once a function is done executing,
    program flow continues at the line after the function call
    was made.

\subsection{ The coordinate plane }

    In computer graphics, $(x,y)$ coordinates are used to
    place images and text on the screen. You might remember
    coordinate planes from algebra. While the same idea
    applies here, the coordinate plane is laid out a bit differently.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[width=0.7\textwidth]{images/coordinateplane.png}
            \\ \footnotesize The coordinate plane, with (0,0) at the top-left corner.
        \end{center}
    \end{figure}

    ~\\ In computer graphics, the origin of the coordinate plane is
    commonly found at $(0, 0)$. As you move to the
    \textbf{right}, the $x$ value increases, and
    as you move \textbf{down}, the $y$ coordinate increases.

\subsection{ Variables }

    Variables are \textbf{ locations } in memory where we can store \textbf{ data }.
    This data can be as simple as numbers, or a string of text, or it
    can be more complicated, such as a \textit{ coin-object }
    or a \textit{ non-player-character-object}.

    As an example, each of our game characters will have an
    $ (x, y) $ coordinate so that the game knows where to
    draw the character to on the screen. A character might
    also have a \textit{ score } variable, or a
    \textit{ name } variable, or many other things.

    It is better to use variables rather than hard-coding
    values throughout your entire program. It is easier
    to go back and make fixes or changes if the data is
    stored in variables - What if your character's starting
    position in the level was wrong and you had to go back
    and update those coordinates in \textit{ every level of the game}?

    There are also rules you have to follow when naming your
    variables. You cannot have spaces in a variable name,
    you cannot start a variable name with a number (though it
    may \textit{contain} numbers), and it cannot be a keyword
    (like \textit{if}).

\subsection{ Functions }

    Functions are collections of instructions that can be run.

    A function has a name (like ``AddToScore"), which can
    be used over and over. This way, you don't have to write
    the same code every time you want to do the task: you
    just call the function!

    Functions can have \textbf{ inputs } and \textbf{ outputs}.
    Inputs are usually called \textbf{ parameters } and
    outputs are called the \textbf{ return value}.

    For example, if we had an \texttt{ AddToScore } function,
    its input might be \textit{ ``How much do we add to the score?" }
    and its output could be \textit{ ``What is the score after we add this amount?" }

    In code form, it would look something like this in Python:

% code box %
\begin{lstlisting}[style=pycode]
def AddToScore( amount ):   # amount is the input
score = score + amount  # adding onto score variable
return score            # returning the result
\end{lstlisting}
% code box %

    Functions don't \textit{always} need to have inputs or
    outputs, it depends on what the function's task is.

\subsection{ Classes }

    Classes are a way that we, the programmers, can create
    our own variable \textbf{data-types}. By default,
    we can use variables like \textbf{integers} (whole numbers) and
    \textbf{strings} (text in double-quotes), but if we
    want to make a game character that has multiple attributes,
    such as \textit{name, coordinates, level}, etc.,
    we can write a \textbf{class} to do this.

    A class can contain \textbf{member functions and variables},
    so that we can have functionality and attributes associated
    with our object.


\newpage{}

\begin{framed}
    ~\\
    \textbf{ Bunny object } \\

    \begin{wrapfigure}{r}{0.3\textwidth}
        \includegraphics[width=0.3\textwidth]{images/bunny2.png}
    \end{wrapfigure}
    
    ~\\
    \textbf{ Variables: } 
    \begin{itemize}
        \item \texttt{score} (integer)
        \item \texttt{xCoordinate} (integer)
        \item \texttt{yCoordinate} (integer)
        \item \texttt{name} (string)
    \end{itemize}

    ~\\
    \textbf{ Functions: }
    \begin{itemize}
        \item \texttt{Move( direction )}
        \item \texttt{AddToScore( amount )}
        \item \texttt{DrawToScreen( window )}
    \end{itemize}
    
\end{framed}

    The great thing about \textbf{classes} is that, once
    you've defined one class, you can create as many variables
    of that class type as you want.

    In other words, once you implement a \textbf{Player}
    class, you could reuse the same code to make your
    \textit{player1, player2, player3,} and \textit{player4}!



\end{document}

