\documentclass[../Intro-to-Pygame.tex]{subfiles}
 
\begin{document}

\chapter{Getting ready}

\section{About this guide}
    
    \begin{wrapfigure}{r}{151px}
        \includegraphics[width=144px]{images/keypress.png}
    \end{wrapfigure}

    \paragraph{What does this guide cover?} ~\\
    This guide covers the basic syntax and usage of the Python language
    in case you are coming from another language. If you've already
    spent time programming in Python, you can skip the \underline{``Python Crash Course"}
    section.
    ~\\~\\
    This guide also covers the PyGame basics that you will need in order
    to build a functioning 2D game. The \underline{``PyGame Crash Course"} runs
    through all the basic functionality pretty quickly, complete with
    example code.
    ~\\~\\
    After we get the basics of Python and PyGame down, we will dive into
    building a few simple 2D games in the \underline{``Building Simple Games"} chapter
    ~\\~\\
    And at the end of this guide, you will find \underline{quick reference guides}
    for both Python and PyGame, so that if you forget something and need
    to look it up, you can do so quickly without sifting through the main guides.
    
\section{Prerequisites}

    \paragraph{What prior experience do I need?} ~\\
    This guide works best if you have already had some prior programming
    experience in an object oriented language, such as C++, C\#, and/or Java.
    In the ``Python Crash Course" chapter, I go over some of the basics of
    Python so that you can be familiar with its syntax, but it is
    \textit{not} an introductory programming course and it doesn't
    cover theory and the basics you would have had in a previous course.

    \paragraph{What if I haven't programmed before?} ~\\
    You might be able to get started with 2D programming using just
    this guide if you \textit{haven't} had any programming classes
    before, but you will hit a wall on the difficulty of problems
    that you can solve -- but don't worry, there are plenty of free
    guides online that you can use to study up!
    

\section{What \textit{is} Python and PyGame?}

    \begin{wrapfigure}{r}{0.4\textwidth} \begin{mdframed}[backgroundcolor=intro]
    The lower-level you get, the closer you are to machine language...
    which is literally just binary.
    \end{mdframed} \end{wrapfigure}
    
    \paragraph{Python} is an interpreted high-level programming language.
    In this case, ``high level" means \textit{further from machine code}.
    ``High level" doesn't mean it is \textit{harder} - actually, high-level
    languages are generally easier to use than low-level languages like C.
    As a language, Python can be written in an
    \underline{Object-Oriented Style} (which should be familiar
    if you're from the C++/C\#/Java side of things), but
    it also supports other programming \textit{paradigms} - or
    other ways of structuring and writing code.
    \textit{I} prefer writing games using Object Oriented techniques.
    
    \paragraph{PyGame} is a \underline{library} that can be used
    with Python.
    A \underline{library} is pre-written code that can be
    reused in multiple programs. This usually includes
    \underline{functions} and \underline{classes} that
    provide new functionality.

    ~\\PyGame handles features like: ~\\

    % I just want this list to be tight...%
    \begin{tabular}{l l}
        \textbullet \tab[0.2cm] Loading and drawing graphics
        &
        \textbullet \tab[0.2cm] Loading and playing sounds
        \\
        \textbullet \tab[0.2cm] Loading fonts and drawing text
        &
        \textbullet \tab[0.2cm] Detecting keyboard and mouse input
    \end{tabular}

    Additionally, PyGame is built to be \underline{cross-platform},
    so you can write a game on your machine, and your friends can
    run the game on Linux, Mac, Windows, or even mobile devices
    \footnote{Building your project for mobile devices requires a bit more work
    than just targeting desktop computers, so we won't be covering that
    in this guide!}!
        
\section{Setting up Python \& PyGame}

    \subsection*{ Downloading PyGame }
    The PyGame website has the downloads for PyGame,
    as well as a tutorial on getting started, documentation
    for PyGame, and even a directory of games made with
    PyGame. Once you create a game, you can put your game on
    the directory, too!
    The PyGame website is at

    \begin{center}
        \texttt{ pygame.org }
    \end{center}

    \subsubsection*{ Windows }

    For Windows, you will download a version of PyGame from
    here: 
    \begin{center} \texttt{ http://pygame.org/download.shtml } \end{center} 

    Look for the ``Windows" header and download the version
    of PyGame that contains ``py2.7" in the title. At the
    time of writing, the latest version is
    \texttt{ pygame-1.9.1.win32-py2.7.msi }.

    \begin{error}{Python versions}
    Version 2 and Version 3 of Python have a lot of
    differences and are not always compatible between each
    other. This version of PyGame uses Python 2.7.    
    \end{error}

\begin{TODO} VERIFY THAT DOWNLOADING PYTHON SEPARATELY ISN'T REQUIRED \end{TODO}

    \newpage
    \subsubsection*{ Linux }

    \begin{wrapfigure}{l}{0.4\textwidth}
        \includegraphics[width=0.4\textwidth]{images/linux-pygame-install.png}
    \end{wrapfigure}
    
    If you’re using Ubuntu, Linux Mint, or Debian, you should
    be able to install Python via the Synaptic Package Manager,
    or even the Software Manager.

    Search for ``pygame", and you should download the
    \texttt{ python-pygame } package. It should also install
    the Python dependencies at the same time.

    \subsubsection*{ Mac }

    Download PyGame from here:

    \begin{center}
        \texttt{ http://pygame.org/download.shtml }
    \end{center}
    
    Look for the ``Macintosh" header and download the version for py2.7.

\begin{TODO} ADD MAC STEPS \end{TODO}

\subsection*{ Downloading a text editor }

    Source code is all text, so you will need a decent text
    editor to write with. The Windows default Notepad is a
    terrible program, so you should download something else.

    \begin{wrapfigure}{l}{0.1\textwidth}
        \includegraphics[width=0.1\textwidth]{images/geany.png}
    \end{wrapfigure} 

    ~\\ \textbf{ Geany } \\
    Geany is a free, cross-platform editor. You can also install
    additional plugins into Geany to customize it.
    Download it at: \\
    \texttt{ http://www.geany.org/Download/Releases }

\newpage       
\section{Testing it out}

    Let's make sure that Python and PyGame are working correctly
    with some small example programs.
    You can re-type the code given in this document, or you can download
    the files from the guide repository at
    \begin{center}
        \texttt{github.com/Rachels-Courses/Intro-to-PyGame}
    \end{center}
    
    \subsection{Making sure Python works}

    First, create a directory on your computer for your Python
    projects, and create a folder for this project (``Testing Python" or something).
    First we will build just a Python program, and then we will build
    a PyGame program.
        
    In Geany, create a new file, and save it to your project
    directory as \texttt{simple\_python.py}. Make sure it
    has the \texttt{.py} extension - all Python source
    files must end with this
    \footnote{If you're in Windows, your folder options might be
    set so that file extensions are hidden by default. You might want
    to set all extensions to be visible so that you don't get confused!}.

    Add the following code into your source file:
    ~\\

% code box %
\begin{lstlisting}[style=pycode]
print( "Hello, world!" )

for i in range( 1, 10 ):
print( i )
\end{lstlisting}
% code box %

    \begin{figure}[h]
        \begin{center}
            \footnotesize{Don't type out the line numbers - those are just for reference.}
        \end{center}
    \end{figure}

    \paragraph{If you're using Geany,} you can run the
    Python program by hitting \texttt{F5}
    
\begin{TODO} VERIFY THAT THIS WORKS IN ALL OPERATING SYSTEMS \end{TODO}

    \paragraph{If you're not using Geany,} then you can
    run your Python program directly from the command-line instead.

\newpage
    \subsubsection*{Opening the terminal}

    The terminal (otherwise known as the command-line, command-prompt, or sometimes DOS)
    is a text-based interface that you can use to run programs.
    You can start up your Python and PyGame projects from the terminal
    using a simple command.

    \paragraph*{Opening the command-line in Windows}

    From within your project folder, hold down your \textit{SHIFT} key
    and then right-click in some empty space. ``Open in prompt" should
    be an option, and it will open up the command prompt in this directory.

    \begin{TODO} FIX UP THIS PART \end{TODO}
    
    \paragraph*{Opening the command-line in Linux}
    
    ~\\ In many desktop environments, you can right-click
    within the folder that you're in to open up
    the terminal in that directory.

    \begin{figure}[h]
        \begin{center}
            \includegraphics[width=0.5\textwidth]{images/openinterminal.png}
            \\ \footnotesize Opening up the Terminal in Linux Mint
        \end{center}
    \end{figure}
    
    \paragraph*{Running from the command-line} ~\\

    Once you have the Terminal open, type:

    \begin{center}
    \texttt{ python simple\_python.py }
    \end{center}

    and then hit \texttt{ ENTER } to run the program.

    \paragraph*{ Example output } ~\\

        Once it has run, the Python program output should
        look like this:


% program output %
\begin{figure}[h]

\begin{lstlisting}[style=output]
Hello, world!
1
2
3
4
5
6
7
8
9
\end{lstlisting}
\begin{center}
\footnotesize A Python program that says ``Hello, world!" and the numbers 1 through 9.
\end{center}
\end{figure}
% program output %


\subsection{Making sure PyGame works}

    Next we will create a simple PyGame test to make sure we
    can get a window open. Create another source file named
    \texttt{simple\_pygame.py}.
    Paste in the following code. Don't worry if you don't
    understand the code yet.
    
% code box %
\begin{lstlisting}[style=pycode]
import pygame, sys
from pygame.locals import *

pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out PyGame!" )

# Game loop
while True:
    window.fill( pygame.Color( 50, 200, 255 ) )

    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            pygame.quit()
            sys.exit()

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
\end{lstlisting}
% code box %

    \newpage
%\begin{wrapfigure}{l}{100px}
%\includegraphics[width=100px]{images/smallwin.png}
%\\ \footnotesize A small window filled with light blue. Its title bar says “Testing out PyGame!”
%\end{wrapfigure}
    
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.3\textwidth]{images/smallwin.png}
\\ \footnotesize A small window filled with light blue. Its title bar says “Testing out PyGame!”
\end{center}
\end{figure}
    
    Once run, it should pop up a little blue window, with the terminal window in the background.
    You can close the window once you're done.

    \paragraph{It's not working!} ~\\
    Sometimes this happens! If you're having trouble running the program,
    there are a few things you can do to check...

    \begin{itemize}
        \item Double-check all text and symbols for typos - these kinds
        of errors are easy to make!
        \item Download the class files and try running my version instead.
        \item Do a search for the error message you're getting -- chances
        are that somebody else has had the same problem!
    \end{itemize}

    

    \paragraph{Where are the class files?} ~\\
    You can download the files, example code, and even this
    document (and the \LaTeX\ file) from the GitHub repository.
    By clicking on ``Clone or download", you can download all
    the files in a ZIP file.
    
    \begin{center}
        \texttt{ https://github.com/Rachels-Courses/Intro-to-PyGame }
    \end{center}

    \paragraph{What's next?} ~\\
    Good work! Next we will run through some of the features of Python
    so you can become familiar with its syntax!


\end{document}

