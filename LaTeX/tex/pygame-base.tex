\documentclass[../Intro-to-Pygame.tex]{subfiles}
 
\begin{document}


    \section{ Bare-minimum PyGame program }

        For the time being, we will store all of our source code in
        one .py file. Once your projects get larger, it is good to
        break out code into separate files, such as everything
        related to a given class in its own .py file.

        When creating a new project in PyGame, all you need is one
        source file, but it is good to create a folder to contain
        the source code and all your images, fonts, and audio files in.

        Usually, I’ll create a folder named \textbf{ content } or
        \textbf{ assets } to
        store these multimedia files in, and keep the source code
        at the base level of my project folder. For example:

            \begin{figure}[h]
                \begin{center}
                    \includegraphics[width=0.5\textwidth]{images/openinterminal.png}
                    \\ \footnotesize Opening up the Terminal in Linux Mint
                \end{center}
            \end{figure}

        To access items within these paths, we use \textbf{ relative pathing. }
        Anything in the base folder (for me, ``03 Creating a PyGame project")
        is at the current directory – if you wanted to load an image from
        this location, you would just specify \texttt{ "myimage.png" }.
        If you want to load an image from the ]textbf{ graphics } folder,
        then you’d include the two folders:
        \texttt{ "content/graphics/myimage.png" }.

        You can also use absolute pathing, where you write out the entire
        path (e.g., \texttt{ "C:\textbackslash pictures\textbackslash doggie.png" }), but it is
        better to store all your project files in the same directory for easier access.

        Within my graphics folder, I will be using the following files.
        You can download the images from the GitHub repository in
        the \textbf{ Assets } folder.

        
            \begin{figure}[h]
                \begin{center}
                    \includegraphics{images/bunny.png}
                    \\ \footnotesize bunny.png
                \end{center}
            \end{figure}

            
            \begin{figure}[h]
                \begin{center}
                    \includegraphics[width=0.5\textwidth]{images/grass.png}
                    \\ \footnotesize grass.png
                \end{center}
            \end{figure}

            Save these images in your project folder and create your .py source file and then continue.

        \subsection{ Basics of a PyGame program }

            For a very basic PyGame program, we will at least need to do the following:

            \begin{enumerate}
                \item Import the PyGame files
                \item Initialize the window
                \item Initialize the timer
                \item Create a game loop
                \item Check for input
                \item Draw to the screen
                \item Update the screen
                \item Use the timer to regulate the framerate
            \end{enumerate}

            We will cover loading files more in-depth later on,
            but for now let’s look at the parts of my basic PyGame template...

        \subsection{ Importing libraries }

            A \textbf{ library } is code that has already been
            written ahead of time, and is packaged up so that it
            can be used in multiple programs. There are a lot of
            libraries for Python, and you can use multiple libraries
            at a time, but for now we just want the PyGame library.

% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *
\end{lstlisting}
% code box %

            To pull in another library in Python, we use the \texttt{ import }
            command. Pygame has a bunch of functions we can use to
            load in graphics, handle program speed, detect input,
            and more. The \texttt{ pygame.locals } part contains shortcut
            named constants that are used as labels for event types,
            keyboard keys, and more. For now, you can take it for granted.

        \subsection{ Initializing PyGame }

            To initialize PyGame, we need to use the following functions:

% code box %
\begin{lstlisting}[style=pycode]
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 320, 320 ) )
pygame.display.set_caption( "Testing out PyGame!" )
\end{lstlisting}
% code box %

            Here, \texttt{ timer } and \texttt{ window } are \textbf{ variables }.
            Variables are
            locations in memory where we can store data, and in
            this case the \texttt{ timer } variable is an object that is
            responsible for making sure we have a constant
            frame-rate, and the \texttt{ window } variable is where we will
            draw our text and graphics to. In Python, we don’t
            need to declare variables before we use them (like we
            would in C++, Java, or C\#) – to create a variable,
            we just start using it. When we assign a value to the
            variable, it will figure out what the data type is on its own.

            \texttt{ pygame.init() },
            \texttt{ pygame.display.set\_mode( ... ) },
            and \\
            \texttt{ pygame.display.set\_caption( ... ) }
            are all \textbf{ function calls }.
            These are functions that are part of PyGame.
            Functions perform actions for us, though we
            don’t need to know exactly how it works behind-the-scenes.
            
            Within the parentheses of the functions we pass in \textbf{ arguments },
            which are essentially the inputs of the function.
            For example, in the \texttt{ set\_mode } function,
            it takes in a \textbf{ width } and
            \textbf{ height } value, and it will set the screen to these dimensions.
            In this case, I’ve hard-coded it to 320 $ \times $ 320 pixels.

            The \texttt{ set\_caption } function takes in a string, or a string of text.
            A string literal must be contained within double-quotes.
            This function sets the window’s title bar text.

            If you ran just the initialization code, a window
            would pop up and immediately close – that’s because
            the program will read from top-to-bottom, and once it
            hits the bottom of the source file it will quit. This
            is where a game loop comes in.



        \subsection{ The basic game loop }

            We want to make sure that the program keeps running
            until the user decides to quit. In order to do this,
            we can use a \textbf{ while loop }.

% code box %
\begin{lstlisting}[style=pycode]
done = False
while done == False:
# Contents of the game loop
\end{lstlisting}
% code box %

            \textbf{ Contents of a code block are indented } ~\\
            In Python, the contents of a function, if statement, or
            loop must be indented by one level. In C++, you might
            be used to these internal code-blocks being contained in
            curly braces $ \lbrace  \rbrace $, but this rule is different for Python.

            As another example to illustrate this is:
            
% code box %
\begin{lstlisting}[style=pycode]
print( "Before the if statement" )

if ( a == b ):
print( "Inside the if statement" )

print( "Outside the if statement" )
\end{lstlisting}
% code box %

            Within the if statement, the print( ``Inside the if statement" )
            is indented forward by one level. Once the if statement is done,
            we indent backward once. This is required, as part of Python’s syntax.

        \subsection{ Detecting a quit (inside the game loop) }

            Since we are sitting inside of a while loop, each cycle
            something might happen. You might update your character
            to move forward by a few pixels, or a timer might go down,
            or the user might hit one or more keys.

            In order to detect user input, we iterate through all of
            the events, which are captured by PyGame. If we detect a
            \texttt{ QUIT } event, we know the user wants to quit – they’ve hit
            the ``X" button in the corner.

            Note that this code is inside the game loop, so the for
            loop is indented forward by one tab.
            
% code box %
\begin{lstlisting}[style=pycode]
# (Inside game loop...)
# Check input
for event in pygame.event.get():
    if ( event.type == QUIT ):
        done = True
\end{lstlisting}
% code box %


        \subsection{ Updating the screen (inside the game loop) }
        
            At the end of the game loop cycle, we will want to update
            the screen so it will actually re-draw everything to the
            screen. We also want to regulate the framerate so that we
            stick to 30 or 60 frames per second, and the speed of the
            game doesn’t change from computer-to-computer.

            Again, this code is indented forward by one level because
            it is within the game loop.

% code box %
\begin{lstlisting}[style=pycode]
# (Inside game loop...)
# Update screen
pygame.display.update()
timer.tick( 30 )
\end{lstlisting}
% code box %

        \subsection{ Test 1 }

            The code so far should look like this. Right now if you run it,
            the game window will just be a small black screen.

            \begin{figure}[h]
                \begin{center}
                    \includegraphics[width=0.5\textwidth]{images/boring.png}
                    \\ \footnotesize An empty PyGame window with a black background
                \end{center}
            \end{figure}
            
\newpage
% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *

pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out PyGame!" )

# Game loop
done = False
while done == False:
# Check input
for event in pygame.event.get():
    if ( event.type == QUIT ):
        done = True

# Update screen
pygame.display.update()
timer.tick( 30 )
\end{lstlisting}
% code box %

        \newpage
        \subsection{ Cleaning it up a little }

        \begin{wrapfigure}{r}{0.4\textwidth} \begin{mdframed}[backgroundcolor=intro]
        To \textbf{ Refactor } means to spend time cleaning up your
        code \textit{ without } modifying its functionality - not
        adding any new features, simply cleaning up.
        \end{mdframed} \end{wrapfigure}

        ~\\
        
            It is good to organize your code, and periodically go
            back and \textbf{ refactor } it to clean it up. If you never clean
            up your code, it will get messier and messier and messier,
            and be difficult to maintain or to keep adding on it.

            Additionally, it is better to use variables for data
            instead of hard-coding it in your function calls. This
            means you only have to update the data in one location
            (the variable assignment), and you won’t have to go update
            a bunch of areas if you decide to change the data later on.

            So, let’s clean up the code above and add some variables
            to store information about the program.

        ~\\ \textbf{ Top of the program }

% code box %
\begin{lstlisting}[style=pycode]
import pygame, sys
from pygame.locals import *

# Global variables
screenWidth = 300
screenHeight = 300
timer = None
window = None
fps = 30
\end{lstlisting}
% code box %

            Now if we need to know the dimensions of our game window,
            we simply need to use the \texttt{ screenWidth } and
            \texttt{ screenHeight } variables,
            instead of hard-coding 300 at every location.

            ~\\ ~\\
            Now we can change the initialization code to use these variables:

% code box %
\begin{lstlisting}[style=pycode]
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
pygame.display.set_caption( "Testing out PyGame!" )
\end{lstlisting}
% code box %
            ~\\ 
            And at the end of the game loop, make sure to update the \texttt{ timer.tick } function:

% code box %
\begin{lstlisting}[style=pycode]
# (Inside game loop...)
timer.tick( fps )
\end{lstlisting}
% code box %


        ~\\ \textbf{ Creating color }

            Next, create a color so we aren’t just staring at a black
            empty window anymore.
            To create a color, we use the \texttt{ pygame.Color } class.
            The variable we store the data in then becomes an
            \textbf{ object-variable }.

% code box %
\begin{lstlisting}[style=pycode]
# Create a color to use
bgColor = pygame.Color( 50, 200, 255 )
\end{lstlisting}
% code box %

            Within the parentheses, we’re passing in values for
            \textbf{ red },
            \textbf{ green }, and
            \textbf{ blue }. The color above will be a light blue.
            If you want to re-use the same color many times
            (for example, when drawing text), it is good to store
            the color in a variable so you can adjust the color in
            one place without updating it over and over everywhere in the code.
            Within the game loop (right after \texttt{while done is False:}), add the following:

% code box %
\begin{lstlisting}[style=pycode]
# (Inside game loop...)
window.fill( bgColor )
\end{lstlisting}
% code box %

            When you run the program, the game screen should be blue now.

            \begin{figure}[h]
                \begin{center}
                    \includegraphics[width=0.4\textwidth]{images/smallwin.png}
                    \\ \footnotesize An empty game screen, but it is blue this time.
                \end{center}
            \end{figure}

        ~\\ \textbf{ Loading images } ~\\

        We can also load images and store these images in variables.
        When you’re loading images, it should be outside of the game loop.
        Think of all of the code before the \texttt{while done is False:}
        as initialization code.

        Load the images that were saved above with:
        
% code box %
\begin{lstlisting}[style=pycode]
# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )
\end{lstlisting}
% code box %

        ~\\ And then draw them to the screen \textit{within} the game loop with:

% code box %
\begin{lstlisting}[style=pycode]
# (Inside game loop...)
# Draw images
window.blit( imgGrass, imgGrass.get_rect() )
window.blit( imgBunny, imgBunny.get_rect() )
\end{lstlisting}
% code box %

        This should be right before the \texttt{ \# Update screen } area.

        \subsection{ Test 2 }

            Here is the full code, though I have created a function
            called \texttt{ InitPygame } and put my init code within it.

            \begin{figure}[h]
                \begin{center}
                    \includegraphics[width=0.4\textwidth]{images/bunny1.png}
                    \\ \footnotesize An empty game screen, but it is blue this time.
                \end{center}
            \end{figure}
            
% code box %
\begin{lstlisting}[style=pycode]
import pygame, sys
from pygame.locals import *

# Global variables
screenWidth = 300
screenHeight = 300
timer = None
window = None
fps = 30

# Create a color to use
bgColor = pygame.Color( 50, 200, 255 )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

# Function definitions
def InitPygame( screenWidth, screenHeight ):
global window
global timer
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
pygame.display.set_caption( "Testing out PyGame!" )

# Program start
InitPygame( screenWidth, screenHeight )

# Game loop
done = False
while done == False:
window.fill( bgColor )

# Check input
for event in pygame.event.get():
    if ( event.type == QUIT ):
        done = True

# Draw images
window.blit( imgGrass, imgGrass.get_rect() )
window.blit( imgBunny, imgBunny.get_rect() )

# Update screen
pygame.display.update()
timer.tick( fps )
\end{lstlisting}
% code box %

        
        \subsection{ Documentation links }

            \begin{itemize}
                \item \textbf{ pygame.time.Clock } ~\\
                    https://www.pygame.org/docs/ref/time.html\#pygame.time.Clock
                    
                \item \textbf{ pygame.time.Clock.tick } ~\\
                    https://www.pygame.org/docs/ref/time.html\#pygame.time.Clock.tick
                    
                \item \textbf{ pygame.display.set\_mode } ~\\
                    https://www.pygame.org/docs/ref/display.html\#pygame.display.set\_mode
                    
                \item \textbf{ pygame.display.set\_caption } ~\\
                    https://www.pygame.org/docs/ref/display.html\#pygame.display.set\_caption
                    
                \item \textbf{ pygame.display.update } ~\\
                    https://www.pygame.org/docs/ref/display.html\#pygame.display.update
            \end{itemize}

    \hrulefill 

\end{document}

