\documentclass[../Intro-to-Pygame.tex]{subfiles}
 
\begin{document}

\newpage{}

    \section{ Images }

        In the last example we loaded some images, but let’s look
        closer at these functions, and some other image-related
        functions that PyGame provides for us.
        To load an image into the program, you will use the
        \texttt{pygame.image.load} function.

        \subsection{ Load an image }
            \textbf{pygame.image.load}

            \begin{itemize}
                \item \textbf{ Inputs: } filename (string)
                \item \textbf{ Outputs: } image-surface object (store it in a variable)
            \end{itemize}

            When making a \textbf{function call}, any \textbf{inputs} go
            within the parentheses ( ) of the function. Since the
            input here is a string, the path to your image file
            should be contained within double-quotes like
            
            \begin{center}
            \begin{verbatim}
            "content/graphics/blorp.png"
            \end{verbatim}
            \end{center}

            And any \textbf{outputs} should be stored in a variable
            using an \textbf{assignment statement}.
            
            \begin{center}
            \begin{verbatim}
            returnStorage = FunctionCall( inputs )
            \end{verbatim}
            \end{center}

            The call to \texttt{pygame.image.load} will look like this:

% code box %
\begin{lstlisting}[style=pycode]
imgGrass = pygame.image.load( "content/graphics/grass.png" )
\end{lstlisting}
% code box %

            \paragraph{ Notes: }
            \begin{itemize}
                \item \textbf{ The file path is relative }
                - this means that it’s in relation to the current
                path of the source code file you’re working in.
                For me, \textbf{ content } is a folder at the same level as
                my .py file. Use forward-slashes, /, to differentiate
                between folders; the \textbf{ graphics } folder is inside the
                content folder, and the grass.png image is within the graphics folder.
                
                \item \textbf{ Store the result of the function in a variable }
                - otherwise, it will load the image but you won’t
                be able to access it! Here, we are storing it in
                the variable \texttt{ imgGrass } using the assignment operator, =.

                \item \textbf{ String-literals belong in double-quotes } -
                when you're hard-coding the path of an image to load,
                you need to make sure to write the path within double quotes.
                If text is \textit{not} within double-quotes, Python
                will think that it is some sort of command or name, and it
                will give you errors.
            \end{itemize}
        
        
        \subsection{ Draw an image}
            \textbf{Surface.blit}

            \begin{itemize}
                \item \textbf{ Inputs: } source-surface (pygame.image), destination-rectangle (pygame.Rect)
                \item \textbf{ Outputs: } image-surface object (store it in a variable)
            \end{itemize}

                Remember that we created a variable named \texttt{window}:

% code box %
\begin{lstlisting}[style=pycode]
window = pygame.display.set_mode( ( screenWidth, screenHeight ) )
\end{lstlisting}
% code box %

                ~\\ The window variable is technically a \texttt{ pygame.Surface } object.
                To draw our image to the screen, we use the \texttt{ window.blit} function,
                passing in the image variable and a rectangle, which contains
                the $(x,y)$ coordinates, as well as width and height.

% code box %
\begin{lstlisting}[style=pycode]
window.blit( imgGrass, imgGrass.get_rect() )
\end{lstlisting}
% code box %

                Drawing an image like this will place it at the origin
                of the screen, at $(0,0)$. For computers, the origin
                is at the \textit{top-left} corner of the screen,
                which is different from how you may remember
                coordinate planes in algebra. (See 1.3.2 on
                The coordinate plane for more info)

                We can change the position where an image is drawn
                by changing the rectangle input.                    
                If you want to be able to modify the position of your
                image while the program is running, it makes more sense
                to store this information in a variable.

                ~\\ During the game initialization, you could set up a
                rectangle like this:

% code box %
\begin{lstlisting}[style=pycode]
# Near beginning of program
# ( x, y, width, height )
bunnyRect = pygame.Rect( 200, 100, 64, 64 ) 
\end{lstlisting}
% code box %

                ~\\ And then update your \texttt{window.blit} to use this
                rectangle variable:

% code box %
\begin{lstlisting}[style=pycode]
# Inside game loop
window.blit( imgBunny, bunnyRect )
\end{lstlisting}
% code box %

                ~\\ Eventually, we will create a class for our characters,
                where their coordinates are stored as a variable,
                and we will use this for the draw function.


        \subsection{ Draw part of an image }
        
            \begin{wrapfigure}{l}{0.2\textwidth}
                \includegraphics[width=0.2\textwidth]{images/spritesheet.png}
            \end{wrapfigure} 
        
            Sometimes you might want to draw only a portion of an
            image to the screen. Often, when working with animated
            sprites, all the sprites' frames of animation are stored in one
            image file like this one.
            
            To draw just a portion of the image at a time, we have to call
            the blit function with an extra argument:
            
            \texttt{window.blit( IMAGE, ( X, Y ), SUB\_IMAGE\_RECT )}
            
            Where in the SUB\_IMAGE\_RECT, you will specify the
            $(x,y)$ of the pixel to begin at in the image, as well
            as the width and height of the region you want to draw.
            
            So with this sprite sheet, if we wanted to draw the girl
            in row 3 and column 2, we would draw it like...
            
% code box %
\begin{lstlisting}[style=pycode]
frameWidth = 32
frameHeight = 48
rect.x = 1 * frameWidth
rect.y = 2 * frameHeight
rect.width = frameWidth
rect.height = frameHeight
window.blit( image, ( x, y ), rect )
\end{lstlisting}
% code box %

            
            %\begin{figure}[h] \begin{center}
            %    \includegraphics[width=0.2\textwidth]{images/spritesheet.png}
            %    \\ \footnotesize A sprite sheet of a character
            %\end{center} \end{figure}
            
        
        

\subsection{ Example program }

            Let's see an example of using images and rectangles
            together in a program. 
            Remember that you can also download the source code at \\
            \texttt{https://github.com/Rachels-Courses/Intro-to-PyGame}
 
% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out images!" )
bgColor = pygame.Color( 50, 200, 255 )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

bunnyRect = pygame.Rect( 200, 100, 64, 64 )

# Game loop
done = False
while done == False:
window.fill( bgColor )

# Check input
for event in pygame.event.get():
    if ( event.type == QUIT ):
        done = True
        
    if ( event.type == KEYDOWN ):                
        if ( event.key == K_DOWN ):
            bunnyRect.y = bunnyRect.y + 10

# Draw images
window.blit( imgGrass, imgGrass.get_rect() )
window.blit( imgBunny, bunnyRect )

# Update screen
pygame.display.update()
timer.tick( 30 )
\end{lstlisting}
% code box %




        \subsection{ Documentation links }

            \begin{itemize}
                \item \textbf{ pygame.Surface } ~\\
                    https://www.pygame.org/docs/ref/surface.html
                    
                \item \textbf{ pygame.image } ~\\
                    http://www.pygame.org/docs/ref/image.html
                    
                \item \textbf{ pygame.Rect } ~\\
                    https://www.pygame.org/docs/ref/rect.html
            \end{itemize}



    

\end{document}

