\documentclass[../Intro-to-Pygame.tex]{subfiles}
 
\begin{document}


    
    \newpage
    \section{ Keyboard and mouse input }
    
        As events occur in your game, Python will detect them
        and they will be stored in \texttt{pygame.event.get()},
        which you can then iterate through and check what \textit{type}
        of event it is.
        An event might be clicking the mouse button, or pressing down
        on a key on the keyboard, or even exiting the game.
        When the mouse is clicked, we can also get its $(x,y)$ coordinates 
        so that we can figure out \textit{what} was being clicked - 
        a button? A location to move to? And so on.
        You can also check for joystick/gamepad events, but we will just
        stick to keyboard and mouse for now.
        
        \subsection{Event types}
        
            \begin{tabular}{ | l | l | }
                \hline
                \textbf{Event type}    &   \textbf{Name}           \\ \hline
                Quit program    &   \texttt{ QUIT }    \\ \hline
                & \\ \hline
                Key press down  &   \texttt{ KEYDOWN }     \\ \hline
                Key release      &   \texttt{ KEYUP }    \\ \hline
                
                & \\ \hline

                Mouse move              &   \texttt{ MOUSEMOTION }    \\ \hline
                Mouse button down       &   \texttt{ MOUSEBUTTONDOWN }    \\ \hline
                Mouse button release    &   \texttt{ MOUSEBUTTONUP }    \\ \hline
            \end{tabular}
            
            ~\\
            
            Notice that with the events, there is a different for when
            you're clicking \textit{down} on the mouse button or on
            a keyboard key, and releasing it (or \textit{key-up}.
            There might be multiple reasons for wanting to check for
            one or the other.
            
            Once you've detected what \textit{kind} of event is occuring,
            you can then get more information about what happened,
            such as which mouse button was clicked, or which key was pressed.
            
            You can also use events like this to detect keyboard input,
            but this isn't how you'll want to get input for smooth character
            movement in your game. This would be more for small key presses.
            I'll have more on how to get smooth keyboard movement later
            in this section.
            
            ~\\
            
            Within the game loop, you will first need to check for events
            like this:
            
% code box %
\begin{lstlisting}[style=pycode]
# Game loop
while done == False:
    
    # Check input
    for event in pygame.event.get():
        if ( event.type == MOUSEBUTTONDOWN ):
            print( "click" )
\end{lstlisting}
% code box %

            And then within the loop, you will use if statements to
            figure out (1) what kind of event happened, and
            (2) which button was pressed. \\

        \subsection{Mouse events}
            
            For mouse events, you have will start with the following.
            
% code box %
\begin{lstlisting}[style=pycode]
# Game loop
while done == False:
    
    # Check input
    for event in pygame.event.get():
        if ( event.type == MOUSEBUTTONDOWN ):
            print( "A" )
        
        elif ( event.type == MOUSEBUTTONUP ):
            print( "B" )
        
        elif ( event.type == MOUSEMOTION ):
            print( "C" )
\end{lstlisting}
% code box %

            ~\\
            Within the if statements is where you will look at which
            button was pressed and the mouse position.
            
            \texttt{event.pos} gives you the mouse coordinates (x and y),
            and \texttt{event.button} gives you a number that represents
            which button was clicked.
            
% code box %
\begin{lstlisting}[style=pycode]
# Game loop
while done == False:
    
    # Check input
    for event in pygame.event.get():
        if ( event.type == MOUSEBUTTONDOWN ):
            mouseX, mouseY = event.pos
            print( "Mouse X:" + str( mouseX ) )
            print( "Mouse Y:" + str( mouseY ) )
\end{lstlisting}
% code box %

            ~\\
            With this code, the \texttt{print} statements will just
            write out text to the console, not the game itself,
            but you can use this for some light debugging or to
            experiment with at first.
            
            The $(x,y)$ coordinates also obey the rules of computer graphics,
            where the origin $(0,0)$ is at the top-left corner and increases
            right-ward and down-ward from there.
            ~\\
            
            You can also check \textit{which} button was pressed in the
            following way:
            
% code box %
\begin{lstlisting}[style=pycode]
# Game loop
while done == False:
    
    # Check input
    for event in pygame.event.get():
        if ( event.type == MOUSEBUTTONUP ):
            # Get mouse position
            mouseX, mouseY = event.pos
            print( "Mouse X:" + str( mouseX ) )
            print( "Mouse Y:" + str( mouseY ) )
            
            # Get button
            if ( event.button == 1 ):
                print( "Left click" )
            elif ( event.button == 2 ):
                print( "Middle click" )
            elif ( event.button == 3 ):
                print( "Right click" )
            elif ( event.button == 4 ):
                print( "Scroll up" )
            elif ( event.button == 5 ):
                print( "Scroll down" )
\end{lstlisting}
% code box %

            ~\\
            
            \begin{tabular}{ | l | l | l | }
                \hline
                \textbf{Mouse button}    &   \textbf{Number} & \textbf{Events}          \\ \hline
                Left click          &   \texttt{ 1 }    &  MOUSEBUTTONDOWN, MOUSEBUTTONUP
                \\ \hline
                Middle click        &   \texttt{ 2 }    &  MOUSEBUTTONDOWN, MOUSEBUTTONUP
                \\ \hline
                Right click         &   \texttt{ 3 }    &  MOUSEBUTTONDOWN, MOUSEBUTTONUP
                \\ \hline
                Mouse wheel up      &   \texttt{ 4 }    &  MOUSEBUTTONUP
                \\ \hline
                Mouse wheel down    &   \texttt{ 5 }    &  MOUSEBUTTONUP
                \\ \hline
            \end{tabular}
            
            ~\\
            You can view a full example program using mouse events on 
            the next page.
            
            \newpage
            
% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out text!" )

# Game loop
done = False
while done == False:    
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
        elif ( event.type == MOUSEBUTTONDOWN ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE DOWN EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            
        elif ( event.type == MOUSEBUTTONUP ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE UP EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )
            
            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            elif ( event.button == 4 ):
                print( "\t SCROLL UP" )
            elif ( event.button == 5 ):
                print( "\t SCROLL DOWN" )

        elif ( event.type == MOUSEMOTION ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE MOVE EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
\end{lstlisting}
% code box %
            
            
        \subsection{Keyboard events}
        
            You can detect keyboard input similarly to how you
            detect mouse input, but this isn't the ideal way
            to get game input for smooth character movement.
            That part will be under the \textit{Smooth keyboard input} section.
            
            Detecting keyboard events in this way is fine for small
            keyboard features, maybe like typing in your name or
            hitting a keyboard shortcut,
            but if you're going to have your character move around
            by holding down arrow keys, this is \textit{not} the way to do it.
            
            Once again, we start by iterating through the events,
            detecting whether we have a \texttt{KEYDOWN} or \texttt{KEYUP} event.
            
                
% code box %
\begin{lstlisting}[style=pycode]
    # Check input
    for event in pygame.event.get():
        if ( event.type == KEYDOWN ):
            print( "Key press" )
\end{lstlisting}
% code box %

            ~\\ Then, if we have detected one of these events, we can then ask
            \textit{which} key was pressed:
            
% code box %
\begin{lstlisting}[style=pycode]
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True

        if ( event.type == KEYDOWN ):
            if ( event.key == K_UP ):
                bunnyRect.y = bunnyRect.y - 10
            elif ( event.key == K_DOWN ):
                bunnyRect.y = bunnyRect.y + 10
            elif ( event.key == K_LEFT ):
                bunnyRect.x = bunnyRect.x - 10
            elif ( event.key == K_RIGHT ):
                bunnyRect.x = bunnyRect.x + 10
\end{lstlisting}
% code box %

            ~\\ Some of the key presses you can check for are:
            
\begin{figure}[h]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    %\includegraphics[width=.4\linewidth]{image1}
    \begin{tabular}{ | l | l | }
        \hline
        \textbf{Key}    &   \textbf{Name}           \\ \hline
        Escape key      &   \texttt{ K\_ESCAPE }    \\ \hline
        Space bar       &   \texttt{ K\_SPACE }     \\ \hline
        Return (enter)  &   \texttt{ K\_RETURN }     \\ \hline
        & \\ \hline
        Left shift      &   \texttt{ K\_LSHIFT }    \\ \hline
        Right shift     &   \texttt{ K\_RSHIFT }    \\ \hline
        & \\ \hline
        Left control    &   \texttt{ K\_LCTRL }    \\ \hline
        Right control   &   \texttt{ K\_RCTRL }    \\ \hline
        & \\ \hline
        Left alt        &   \texttt{ K\_LALT }    \\ \hline
        Right alt       &   \texttt{ K\_RALT }    \\ \hline
    \end{tabular}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
    \centering
    % \includegraphics[width=.4\linewidth]{image1}
    \begin{tabular}{ | l | l | }
        \hline
        \textbf{Key}    &   \textbf{Name}           \\ \hline
        ``A" key        &   \texttt{ K\_a }    \\ \hline
        ``B" key        &   \texttt{ K\_b }    \\ \hline
        ``C" key        &   \texttt{ K\_c }    \\ \hline
        & \\ \hline
        ``F1" key       &   \texttt{ K\_F1 }        \\ \hline
        ``F2" key       &   \texttt{ K\_F2 }        \\ \hline
        ``F3" key       &   \texttt{ K\_F3 }        \\ \hline
        & \\ \hline
        Up arrow        &   \texttt{ K\_UP }        \\ \hline
        Down arrow      &   \texttt{ K\_DOWN }      \\ \hline
        Left arrow      &   \texttt{ K\_LEFT }      \\ \hline
        Right arrow     &   \texttt{ K\_RIGHT }     \\ \hline
    \end{tabular}
\end{subfigure}
%\caption{A figure with two subfigures}
%\label{fig:test}
\end{figure}

            ~\\ and you can get a list of all the keycodes at \\
            \texttt{ https://www.pygame.org/docs/ref/key.html }
            
            
            
        \subsection{Smooth keyboard input}
        
            If your character is going to move by holding down a key on
            the keyboard, such as the arrow keys or WASD, you will
            want to use this technique instead.
            
            Instead of dealing with key presses as \textit{events},
            we use \texttt{pygame.key.get\_pressed} to see which
            keys are currently down.

% code box %
\begin{lstlisting}[style=pycode]
# In the game loop...
    # Smooth keyboard movement
    keys = pygame.key.get_pressed()
\end{lstlisting}
% code box %

            ~\\ Then, using the keycodes above, we can detect which
            keys are currently being held down. This will also work
            to check more than one key at once (like holding Run + Move).

% code box %
\begin{lstlisting}[style=pycode]
# In the game loop...
    # Smooth keyboard movement
    keys = pygame.key.get_pressed()
    if ( keys[ K_UP ] ):
        bunnyRect.y = bunnyRect.y - 5
    elif ( keys[ K_DOWN ] ):
        bunnyRect.y = bunnyRect.y + 5
    elif ( keys[ K_LEFT ] ):
        bunnyRect.x = bunnyRect.x - 5
    elif ( keys[ K_RIGHT ] ):
        bunnyRect.x = bunnyRect.x + 5
\end{lstlisting}
% code box %

            ~\\
            

        \newpage
        \subsection{ Example - mouse input }
            Remember that you can also download the source code at \\
            \texttt{https://github.com/Rachels-Courses/Intro-to-PyGame}
        
% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out events!" )

# Game loop
done = False
while done == False:    
    # Check input
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
        elif ( event.type == MOUSEBUTTONDOWN ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE DOWN EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            
        elif ( event.type == MOUSEBUTTONUP ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE UP EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )
            
            if ( event.button == 1 ):
                print( "\t LEFT CLICK" )
            elif ( event.button == 2 ):
                print( "\t MIDDLE CLICK" )
            elif ( event.button == 3 ):
                print( "\t RIGHT CLICK" )
            elif ( event.button == 4 ):
                print( "\t SCROLL UP" )
            elif ( event.button == 5 ):
                print( "\t SCROLL DOWN" )

        elif ( event.type == MOUSEMOTION ):
            mouseX, mouseY = event.pos
            print( "" )
            print( "MOUSE MOVE EVENT at (" + str(mouseX) + ", " + str(mouseY) + ")" )

    # Update screen
    pygame.display.update()
    timer.tick( 30 )
\end{lstlisting}
% code box %
        
        \newpage
        \subsection{ Example - smooth keyboard input }
            Remember that you can also download the source code at \\
            \texttt{https://github.com/Rachels-Courses/Intro-to-PyGame}
        
% code box %
\begin{lstlisting}[style=pycode]
import pygame
from pygame.locals import *

# Global variables
pygame.init()
timer = pygame.time.Clock()
window = pygame.display.set_mode( ( 300, 300 ) )
pygame.display.set_caption( "Testing out smooth keyboard input!" )

# Load graphics
imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgBunny = pygame.image.load( "content/graphics/bunny.png" )

bunnyRect = pygame.Rect( 200, 100, 64, 64 )

done = False
while ( done == False ):    
    # Events
    for event in pygame.event.get():
        if ( event.type == QUIT ):
            done = True
            
    # Smooth keyboard movement
    keys = pygame.key.get_pressed()
    if ( keys[ K_UP ] ):
        bunnyRect.y = bunnyRect.y - 5
    elif ( keys[ K_DOWN ] ):
        bunnyRect.y = bunnyRect.y + 5
    elif ( keys[ K_LEFT ] ):
        bunnyRect.x = bunnyRect.x - 5
    elif ( keys[ K_RIGHT ] ):
        bunnyRect.x = bunnyRect.x + 5
    
    # Draw images
    window.blit( imgGrass, imgGrass.get_rect() )
    window.blit( imgBunny, bunnyRect )

    pygame.display.update()
    timer.tick( 30 )
\end{lstlisting}
% code box %
        
        \subsection{ Documentation links }

            \begin{itemize}
                \item \textbf{ pygame.event } ~\\
                    http://www.pygame.org/docs/ref/event.html
                    
                \item \textbf{ pygame.key } ~\\
                    https://www.pygame.org/docs/ref/key.html
                    
                \item \textbf{ pygame.mouse } ~\\
                    https://www.pygame.org/docs/ref/mouse.html
            \end{itemize}


\end{document}

