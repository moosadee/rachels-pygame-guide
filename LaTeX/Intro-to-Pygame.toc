\contentsline {paragraph}{Hey there!}{1}
\contentsline {paragraph}{What is Python and PyGame?}{1}
\contentsline {paragraph}{Wait... what's a library??}{1}
\contentsline {paragraph}{OK, what's up?}{1}
\contentsline {chapter}{\numberline {1}Getting ready}{6}
\contentsline {section}{\numberline {1.1}About this guide}{6}
\contentsline {paragraph}{What does this guide cover?}{6}
\contentsline {section}{\numberline {1.2}Prerequisites}{7}
\contentsline {paragraph}{What prior experience do I need?}{7}
\contentsline {paragraph}{What if I haven't programmed before?}{7}
\contentsline {section}{\numberline {1.3}What \textit {is} Python and PyGame?}{7}
\contentsline {paragraph}{Python}{7}
\contentsline {paragraph}{PyGame}{7}
\contentsline {section}{\numberline {1.4}Setting up Python \& PyGame}{8}
\contentsline {section}{\numberline {1.5}Testing it out}{10}
\contentsline {subsection}{\numberline {1.5.1}Making sure Python works}{10}
\contentsline {paragraph}{If you're using Geany,}{10}
\contentsline {paragraph}{If you're not using Geany,}{10}
\contentsline {subsection}{\numberline {1.5.2}Making sure PyGame works}{12}
\contentsline {paragraph}{It's not working!}{13}
\contentsline {paragraph}{Where are the class files?}{13}
\contentsline {paragraph}{What's next?}{13}
\contentsline {chapter}{\numberline {2}Python Crash Course}{14}
\contentsline {section}{\numberline {2.1}Basic program}{14}
\contentsline {section}{\numberline {2.2}Variables}{14}
\contentsline {section}{\numberline {2.3}Input and output}{14}
\contentsline {section}{\numberline {2.4}If statements}{14}
\contentsline {section}{\numberline {2.5}While loops}{14}
\contentsline {section}{\numberline {2.6}For loops}{15}
\contentsline {section}{\numberline {2.7}Functions}{15}
\contentsline {section}{\numberline {2.8}Classes}{15}
\contentsline {section}{\numberline {2.9}Inheritance}{15}
\contentsline {chapter}{\numberline {3} 2D Game Development Concepts }{16}
\contentsline {section}{\numberline {3.1} Birds-eye-view: How game-program work }{16}
\contentsline {subsection}{\numberline {3.1.1} The coordinate plane }{17}
\contentsline {subsection}{\numberline {3.1.2} Variables }{18}
\contentsline {subsection}{\numberline {3.1.3} Functions }{18}
\contentsline {subsection}{\numberline {3.1.4} Classes }{19}
\contentsline {chapter}{\numberline {4}PyGame Crash Course}{22}
\contentsline {section}{\numberline {4.1} Bare-minimum PyGame program }{22}
\contentsline {subsection}{\numberline {4.1.1} Basics of a PyGame program }{23}
\contentsline {subsection}{\numberline {4.1.2} Importing libraries }{24}
\contentsline {subsection}{\numberline {4.1.3} Initializing PyGame }{24}
\contentsline {subsection}{\numberline {4.1.4} The basic game loop }{25}
\contentsline {subsection}{\numberline {4.1.5} Detecting a quit (inside the game loop) }{26}
\contentsline {subsection}{\numberline {4.1.6} Updating the screen (inside the game loop) }{26}
\contentsline {subsection}{\numberline {4.1.7} Test 1 }{26}
\contentsline {subsection}{\numberline {4.1.8} Cleaning it up a little }{28}
\contentsline {subsection}{\numberline {4.1.9} Test 2 }{30}
\contentsline {subsection}{\numberline {4.1.10} Documentation links }{32}
\contentsline {section}{\numberline {4.2} Images }{33}
\contentsline {subsection}{\numberline {4.2.1} Load an image }{33}
\contentsline {paragraph}{ Notes: }{33}
\contentsline {subsection}{\numberline {4.2.2} Draw an image}{34}
\contentsline {subsection}{\numberline {4.2.3} Draw part of an image }{35}
\contentsline {subsection}{\numberline {4.2.4} Example program }{35}
\contentsline {subsection}{\numberline {4.2.5} Documentation links }{36}
\contentsline {section}{\numberline {4.3} Audio }{38}
\contentsline {subsection}{\numberline {4.3.1} Load a music file }{38}
\contentsline {subsection}{\numberline {4.3.2} Play the music }{38}
\contentsline {subsection}{\numberline {4.3.3} Pause the music }{38}
\contentsline {subsection}{\numberline {4.3.4} Resume the music }{39}
\contentsline {subsection}{\numberline {4.3.5} Set the music volume }{39}
\contentsline {subsection}{\numberline {4.3.6} Get the music volume }{39}
\contentsline {subsection}{\numberline {4.3.7} Create a sound file }{40}
\contentsline {subsection}{\numberline {4.3.8} Play a sound file }{40}
\contentsline {subsection}{\numberline {4.3.9} Set the sound volume }{40}
\contentsline {subsection}{\numberline {4.3.10} Get the sound volume }{40}
\contentsline {subsection}{\numberline {4.3.11} Example program }{41}
\contentsline {subsection}{\numberline {4.3.12} Documentation links }{42}
\contentsline {section}{\numberline {4.4} Fonts and text }{43}
\contentsline {subsection}{\numberline {4.4.1} Loading a font }{43}
\contentsline {subsection}{\numberline {4.4.2} Creating a text Surface }{43}
\contentsline {subsection}{\numberline {4.4.3} Drawing a text Surface }{43}
\contentsline {subsection}{\numberline {4.4.4} Example program }{44}
\contentsline {subsection}{\numberline {4.4.5} Documentation links }{45}
\contentsline {section}{\numberline {4.5} Keyboard and mouse input }{46}
\contentsline {subsection}{\numberline {4.5.1}Event types}{46}
\contentsline {subsection}{\numberline {4.5.2}Mouse events}{47}
\contentsline {subsection}{\numberline {4.5.3}Keyboard events}{50}
\contentsline {subsection}{\numberline {4.5.4}Smooth keyboard input}{51}
\contentsline {subsection}{\numberline {4.5.5} Example - mouse input }{53}
\contentsline {subsection}{\numberline {4.5.6} Example - smooth keyboard input }{55}
\contentsline {subsection}{\numberline {4.5.7} Documentation links }{56}
\contentsline {chapter}{\numberline {5} Building simple games }{57}
\contentsline {section}{\numberline {5.1} Game 1: Whack-a-mole }{57}
\contentsline {subsection}{\numberline {5.1.1}Description}{57}
\contentsline {subsection}{\numberline {5.1.2}Asset list}{58}
\contentsline {subsection}{\numberline {5.1.3}Iteration 1 - Opening the game window}{58}
\contentsline {subsection}{\numberline {5.1.4}Iteration 2 - Drawing holes}{58}
\contentsline {subsection}{\numberline {5.1.5}Iteration 3 - Popping up moles randomly}{58}
\contentsline {subsection}{\numberline {5.1.6}Iteration 4 - Clicking on moles}{58}
\contentsline {subsection}{\numberline {5.1.7}Suggested extra features}{58}
\contentsline {section}{\numberline {5.2} Game 2: Pickin' Sticks }{59}
\contentsline {subsection}{\numberline {5.2.1}Description}{59}
\contentsline {section}{\numberline {5.3} Game 3: Space Invaders }{60}
\contentsline {subsection}{\numberline {5.3.1}Description}{60}
\contentsline {chapter}{\numberline {6} PyGame Quick Reference }{61}
\contentsline {paragraph}{ Bare-minimum program }{61}
\contentsline {paragraph}{ Loading assets }{62}
\contentsline {chapter}{\numberline {7} PyGame Quick Reference }{64}
\contentsline {paragraph}{ Bare-minimum program }{64}
\contentsline {paragraph}{ Loading assets }{65}
\contentsline {chapter}{\numberline {8} Glossary }{67}
